﻿namespace Virtex.Tracing
{
    /// <summary>
    /// Defines a set of standard tracing levels that can be used to control tracing output.
    /// </summary>
    /// <remarks>
    /// The tracing level objects are ordered and are specified by ordered integers. Enabling tracing at a given level
    /// also enables tracing at all higher levels. Clients should normally use the predefined level constants such as
    /// TracingLevel.Severe. The levels in descending order are:
    /// 
    /// Severe(highest value)
    /// Warning
    /// Ingo
    /// Config
    /// Fine
    /// Finer
    /// Finest(lowest value)
    /// 
    /// In addition there is a level Off that can be used to turn off tracing, and a level All that can be used to
    /// enable tracing of all events. It is possible for third parties to define additional tracing levels by inheriting
    /// TracingLevel class.
    /// </remarks>
    public class TracingLevel
    {
        /// <summary>
        /// OFF is a special level that can be used to turn off tracing. This level is initialized to int.MaxValue.
        /// </summary>
        public static readonly TracingLevel Off = new TracingLevel(int.MaxValue, nameof(Off));

        /// <summary>
        /// Severe is a tracing level indicating a serious failure.
        /// </summary>
        /// <remarks>
        /// In general Severe events should describe events that are of considerable importance and which will prevent
        /// normal program execution. They should be reasonably intelligible to end users and to system administrators.
        /// This level is initialized to 1000.
        /// </remarks>
        public static readonly TracingLevel Severe = new TracingLevel(1000, nameof(Severe));

        /// <summary>
        /// Warning is a tracing level indicating a potential problem.
        /// </summary>
        /// <remarks>
        /// In general Warning events should describe events that will be of interest to end users or system managers,
        /// or which indicate potential problems.This level is initialized to 900.
        /// </remarks>
        public static readonly TracingLevel Warning = new TracingLevel(900, nameof(Warning));

        /// <summary>
        /// Info is a tracing level for informational events.
        /// </summary>
        /// <remarks>
        /// Typically Info events will be written to the console or its equivalent. So the Info level should only be
        /// used for reasonably significant events that will make sense to end users and system administrators. This
        /// level is initialized to 800.
        /// </remarks>
        public static readonly TracingLevel Info = new TracingLevel(800, nameof(Info));

        /// <summary>
        /// Config is a tracing level for static configuration events.
        /// </summary>
        /// <remarks>
        /// Config tracing are intended to provide a variety of static configuration information, to assist in debugging
        /// problems that may be associated with particular configurations. For example, Config message might include
        /// the CPU type, the graphics depth, the GUI look-and-feel, etc.This level is initialized to 700.
        /// </remarks>
        public static readonly TracingLevel Config = new TracingLevel(700, nameof(Config));

        /// <summary>
        /// Fine is a message level providing tracing information.
        /// </summary>
        /// <remarks>
        /// All of Fine, Finer, and Finest are intended for relatively detailed tracing. The exact meaning of the three
        /// levels will vary between subsystems, but in general, Finest should be used for the most voluminous detailed
        /// output, Finer for somewhat less detailed output, and Fine for the lowest volume (and most important) events.
        /// In general the Fine level should be used for information that will be broadly interesting to developers who
        /// do not have a specialized interest in the specific subsystem. Fine messages might include things like minor
        /// (recoverable) failures. Issues indicating potential performance problems are also worth tracing as Fine.
        /// This level is initialized to 500.
        /// </remarks>
        public static readonly TracingLevel Fine = new TracingLevel(500, nameof(Fine));

        /// <summary>
        /// Indicates a fairly detailed tracing events.
        /// </summary>
        /// <remarks>
        /// By default tracing calls for entering, returning, or throwing an exception are traced at this level. This
        /// level is initialized to 400.
        /// </remarks>
        public static readonly TracingLevel Finer = new TracingLevel(400, nameof(Finer));

        /// <summary>
        /// Indicates a highly detailed tracing events.
        /// </summary>
        /// <remarks>
        /// This level is initialized to 300.
        /// </remarks>
        public static readonly TracingLevel Finest = new TracingLevel(300, nameof(Finest));

        /// <summary>
        /// Indicates that all events should be traced.
        /// </summary>
        /// <remarks>
        /// This level is initialized to int.MinValue.
        /// </remarks>
        public static readonly TracingLevel All = new TracingLevel(int.MinValue, nameof(All));

        /// <summary>
        /// Initializes a new instance of Level class.
        /// </summary>
        /// <param name="value">Represents an importance of level.</param>
        /// <param name="name">The name of the level.</param>
        protected TracingLevel(int value, string name)
        {
            Value = value;
            Name = name;
        }

        /// <summary>
        /// Gets an integer value representing the importance of this level.
        /// </summary>
        /// <remarks>
        /// This value is used for efficient ordering comparisons between TracingLevel objects. Higher value means
        /// higher importance.
        /// </remarks>
        public int Value { get; }

        /// <summary>
        /// Gets the name of the level.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Returns the <see cref="Name"/> and <see cref="Value"/> of this level.
        /// </summary>
        /// <returns>The <see cref="Name"/> and <see cref="Value"/> of this level.</returns>
        public override string ToString()
        {
            return $"{Name} ({Value})";
        }

        /// <summary>
        /// Determines whether one specified level has the same or lower importance than another specified Level.
        /// </summary>
        /// <param name="fst">The first object to compare.</param>
        /// <param name="snd">The second object to compare.</param>
        /// <returns>true if fst has the same or lower level of importance than snd; otherwise, false.</returns>
        public static bool operator <=(TracingLevel fst, TracingLevel snd)
        {
            return fst.Value <= snd.Value;
        }

        /// <summary>
        /// Determines whether one specified level has the same or higher importance than another specified Level.
        /// </summary>
        /// <param name="fst">The first object to compare.</param>
        /// <param name="snd">The second object to compare.</param>
        /// <returns>true if fst has the same or lower level of importance than snd; otherwise, false.</returns>
        public static bool operator >=(TracingLevel fst, TracingLevel snd)
        {
            return fst.Value >= snd.Value;
        }
    }
}