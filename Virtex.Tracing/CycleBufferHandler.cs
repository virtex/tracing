﻿namespace Virtex.Tracing
{
    /// <summary>
    /// Buffers <see cref="TracingEvent"/> in a circular buffer in memory.
    /// </summary>
    /// <remarks>
    /// The older events are discarded. Buffering of events is very cheap and avoids formatting costs. Handler can push
    /// out its current buffer contents to a target Handler, which will typically publish them to the outside world.
    /// </remarks>
    public class CycleBufferHandler : TracingHandler
    {
        private readonly ConcurrentCycleBuffer<TracingEvent> events;

        private readonly TracingHandler targetHandler;

        /// <summary>
        /// Initializes a new instance of <see cref="CycleBufferHandler"/> class.
        /// </summary>
        /// <param name="targetHandler">The handler to which to publish output.</param>
        /// <param name="bufferSize">The size of circular buffer for the events.</param>
        public CycleBufferHandler(TracingHandler targetHandler, int bufferSize)
        {
            this.targetHandler = targetHandler;
            events = new ConcurrentCycleBuffer<TracingEvent>(bufferSize);
        }

        /// <summary>
        /// Pushes <see cref="TracingEvent"/>s to target handler and flushes the target handler.
        /// </summary>
        public override void Flush()
        {
            Push();
            if (targetHandler != null)
            {
                targetHandler.Flush();
            }
        }

        /// <summary>
        /// Stores tracing event in a circular buffer.
        /// </summary>
        /// <param name="tracingEvent">The tracing event to store.</param>
        public override void Publish(TracingEvent tracingEvent)
        {
            events.Add(tracingEvent);
        }

        /// <summary>
        /// Pushes <see cref="TracingEvent"/>s to target handler.
        /// </summary>
        public void Push()
        {
            if (targetHandler != null)
            {
                PublishEvents(events.GetItems());
            }
        }

        private void PublishEvents(TracingEvent[] traceEvents)
        {
            foreach (var tracingEvent in traceEvents)
            {
                targetHandler.Publish(tracingEvent);
            }
        }

        private class ConcurrentCycleBuffer<T>
        {
            private readonly T[] items;

            private int index = 0;

            private readonly object itemsLock = new object();

            public ConcurrentCycleBuffer(int size)
            {
                items = new T[size];
            }

            public void Add(T item)
            {
                lock (itemsLock)
                {
                    items[index % items.Length] = item;
                    index++;
                }
            }

            public T[] GetItems()
            {
                T[] newItems;

                lock (itemsLock)
                {
                    var begin = index;
                    var end = begin + items.Length;
                    if (index < items.Length)
                    {
                        begin = 0;
                        end = index;
                    }

                    newItems = new T[end - begin];

                    for (int i = begin; i < end; i++)
                    {
                        newItems[i - begin] = items[i % items.Length];
                    }

                    index = 0;
                }

                return newItems;
            }
        }
    }
}