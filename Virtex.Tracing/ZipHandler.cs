﻿using System.IO;
using System.IO.Compression;

namespace Virtex.Tracing
{
    /// <summary>
    /// Stores formatted <see cref="TracingEvent"/>s in zipped file.
    /// </summary>
    public class ZipHandler : FileHandler
    {
        /// <summary>
        /// Gets the extension of a file where events are stored.
        /// </summary>
        protected override string FileExtension => "gz";

        /// <summary>
        /// Initializes a new instance of <see cref="ZipHandler"/> class.
        /// </summary>
        /// <param name="formatter">The formatter for this handler.</param>
        /// <param name="folderPath">The folder where to store zipped files.</param>
        /// <param name="splitCount">The number of formatted <see cref="TracingEvent"/>s per file.</param>
        public ZipHandler(TracingFormatter formatter, string folderPath, int splitCount)
            : base(formatter, folderPath, splitCount)
        {
        }

        /// <summary>
        /// Comress data in given stream and writes it with given writer.
        /// </summary>
        /// <param name="writer">The writer to write data.</param>
        /// <param name="stream">The stream that contains data to write.</param>
        protected override void Write(StreamWriter writer, Stream stream)
        {
            using (var compressionStream = new GZipStream(stream, CompressionLevel.Optimal))
            {
                writer.BaseStream.CopyTo(compressionStream);
            }
        }
    }
}
