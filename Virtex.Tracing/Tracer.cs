﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Virtex.Tracing
{
    /// <summary>
    /// Generates trace events and provides core tracing functionality.
    /// </summary>
    /// <remarks>
    /// Tracers are normally named, using a hierarchical dot-separated namespace. Tracer names should normally be based
    /// on the assembly name, the namespace or class name of the traced component. Tracer objects may be obtained by
    /// call <see cref="GetTracer(string)"/> factory method. These will either create a new tracer or return a suitable
    /// existing tracer. It is important to note that the tracer returned by GetTracer factory method may be garbage
    /// collected at any time if a strong reference to the tracer is not kept.
    /// 
    /// Trace events will be forwarded to registered handlers, which can forward the event to a variety of destinations,
    /// e.g. console and files.
    /// 
    /// Each tracer keeps track of a "parent" tracers, which is its nearest existing ancestor in the tracer namespace.
    /// 
    /// Each tracer has a <see cref="TracingLevel"/> associated with it. This reflects a minimum level that this tracer
    /// cares about. If a tracer's level is set to null, then its effective level is inherited from its parent, which
    /// may in turn obtain it recursively from its parent, and so on up the tree.
    /// 
    /// The tracing level can be dynamically changed by setting the <see cref="Level"/> property. If a tracer's level is
    /// changed the change may also affect child tracers, since any child tracer that has null as its level will inherit
    /// its effective level from its parent.
    /// 
    /// On each tracing call the tracer initially performs a cheap check of the request level (e.g. Fine) against the
    /// effective level of the tracer. If the request level is lower than the tracer level, the tracing call returns
    /// immediately. After passing this initial test, the tracer will allocate a <see cref="TracingEvent"/> to describe
    /// the event and publish it to all its handlers.
    /// </remarks>
    public sealed class Tracer
    {
        /// <summary>
        /// The name of the root tracer.
        /// </summary>
        public static readonly string RootTracerName = string.Empty;

        private static string processName = string.Empty;

        private readonly List<TracingHandler> handlers = new List<TracingHandler>();

        private readonly WeakReference<Tracer> parentReference = new WeakReference<Tracer>(null);

        internal Tracer(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Gets relative time in microseconds since startup or defined era of OS system timer.
        /// </summary>
        /// <remarks>
        /// The returned timestamp is always unique for one thread and mostly unique if called from multiple threads.
        /// If the getter is called more than once a microsecond, returned value does not correspond with real time.
        /// </remarks>
        /// <returns>The timestamp.</returns>
        public static long Timestamp => CalculateTimestamp();

        /// <summary>
        /// Gets or sets the name of the process.
        /// </summary>
        public static string ProcessName
        {
            get => processName;
            set
            {
                processName = value;
                ProcessId = value.GetHashCode();
            }
        }

        /// <summary>
        /// HashCode of <see cref="ProcessName"/> property if it is not empty, otherwise process ID.
        /// </summary>
        public static int ProcessId { get; private set; } = Process.GetCurrentProcess().Id;

        /// <summary>
        /// Gets an instance of the the root tracer.
        /// </summary>
        /// <returns></returns>
        public static Tracer Root => GetTracer(RootTracerName);

        /// <summary>
        /// Gets the name of the tracer.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets or sets the trace Level that has been specified for this tracer.
        /// </summary>
        public TracingLevel Level { get; set; }

        /// <summary>
        /// Gets or sets whether or not events are sent to parent tracer.
        /// </summary>
        public bool UseParentHandlers { get; set; } = true;

        /// <summary>
        /// Gets the nearest extant parent in the tracer's hierarchy or null for root tracer.
        /// </summary>
        /// <remarks>
        /// This property automatically reflects the actual state of tracer's hierarchy, because tracers can be added
        /// and removed in the runtime from the hierarchy.
        /// </remarks>
        public Tracer Parent
        {
            get
            {
                if (!parentReference.TryGetTarget(out Tracer parent))
                {
                    if (Name != RootTracerName)
                    {
                        parent = TracingManager.Instace.FindParent(this);
                        Parent = parent;
                    }
                }
                return parent;
            }
            internal set
            {
                parentReference.SetTarget(value);
            }
        }

        /// <summary>
        /// Gets an instance of the tracer object for given name.
        /// </summary>
        public static Tracer GetTracer(string name) => TracingManager.Instace.GetOrCreateTracer(name);

        /// <summary>
        /// Gets instance of the tracer object named with the namespace of the caller.
        /// </summary>
        /// <returns></returns>
        public static Tracer GetCurrentNamespaceTracer(Type type) => GetTracer(type.Namespace);

        /// <summary>
        /// Gets instance of the tracer object named with the namespace of the caller.
        /// </summary>
        /// <returns></returns>
        public static Tracer GetCurrentNamespaceTracer()
        {
            return GetCurrentNamespaceTracer(new StackTrace().GetFrame(1).GetMethod().DeclaringType);
        }

        /// <summary>
        /// Gets or sets the function that calculates the timestamp.
        /// </summary>
        /// <remarks>
        /// The default function provides number of microseconds since Unix epoch time.
        /// </remarks>
        /// <returns>The timestamp in mircoseconds.</returns>
        public static Func<long> CalculateTimestamp { get; set; } =
            TracingTimestampProvider.GetDateTimeTimestamp;

        /// <summary>
        /// Adds a tracing handler to receive tracing events.
        /// </summary>
        /// <param name="handler">The tracing handler.</param>
        public void AddHandler(TracingHandler handler) => handlers.Add(handler);

        /// <summary>
        /// Removes a tracing handler.
        /// </summary>
        /// <param name="handler">The tracing handler.</param>
        public void RemoveHandler(TracingHandler handler) => handlers.Remove(handler);

        /// <summary>
        /// Removes and flushes all handlers and sets the <see cref="Level"/> to null.
        /// </summary>
        public void Reset()
        {
            foreach (var handler in handlers)
            {
                handler.Flush();
            }
            handlers.Clear();
            Level = null;
        }

        /// <summary>
        /// Flushes any buffered output of all registered handlers.
        /// </summary>
        public void Flush()
        {
            foreach (var handler in handlers)
            {
                handler.Flush();
            }
        }

        /// <summary>
        /// Publishes given event to all registered handlers.
        /// </summary>
        /// <param name="tracingEvent">The event to publish.</param>
        public void Event(TracingEvent tracingEvent)
        {
            if (IsTraceable(tracingEvent.Level))
            {
                Publish(tracingEvent);
            }
        }

        /// <summary>
        /// Creates and publishes an instant event. The instant event has no time interval associated with it.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="name">The name of the event.</param>
        public void InstantEvent(TracingLevel level, string name)
        {
            if (IsTraceable(level))
            {
                Publish(new TracingEvent(level, TracingPhase.Instant, name, Name, GetThreadId()));
            }
        }

        /// <summary>
        /// Creates and publishes an instant event. The instant event has no time interval associated with it.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="name">The name of the event.</param>
        /// <param name="userData">The user data object.</param>
        public void InstantEvent(TracingLevel level, string name, object userData)
        {
            if (IsTraceable(level))
            {
                Publish(new TracingEvent(level, TracingPhase.Instant, name, Name, GetThreadId(), userData));
            }
        }

        /// <summary>
        /// Creates and publishes an instant event. The instant event has no time interval associated with it.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="format">The name of the event in a composite format string.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public void InstantEventFormat(TracingLevel level, string format, params object[] args)
        {
            if (IsTraceable(level))
            {
                Publish(
                    new TracingEvent(level, TracingPhase.Instant, string.Format(format, args), Name, GetThreadId()));
            }
        }

        /// <summary>
        /// Creates and publishes an instant event. The instant event has no time interval associated with it.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="userData">The user data object.</param>
        /// <param name="format">The name of the event in a composite format string.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public void InstantFormatEvent(TracingLevel level, object userData, string format, params object[] args)
        {
            if (IsTraceable(level))
            {
                Publish(
                    new TracingEvent(
                        level, TracingPhase.Instant, string.Format(format, args), Name, GetThreadId(), userData));
            }
        }

        /// <summary>
        /// Creates and publishes a duration event with Begin phase.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="name">The name of the event.</param>
        public void BeginEvent(TracingLevel level, string name)
        {
            if (IsTraceable(level))
            {
                Publish(new TracingEvent(level, TracingPhase.Begin, name, Name, GetThreadId()));
            }
        }

        /// <summary>
        /// Creates and publishes a duration event with Begin phase.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="name">The name of the event.</param>
        /// <param name="userData">The user data object.</param>
        public void BeginEvent(TracingLevel level, string name, object userData)
        {
            if (IsTraceable(level))
            {
                Publish(new TracingEvent(level, TracingPhase.Begin, name, Name, GetThreadId(), userData));
            }
        }

        /// <summary>
        /// Creates a publishes a duration event with End phase.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="name">The name of the event.</param>
        public void EndEvent(TracingLevel level, string name)
        {
            if (IsTraceable(level))
            {
                Publish(new TracingEvent(level, TracingPhase.End, name, Name, GetThreadId()));
            }
        }

        /// <summary>
        /// Creates a publishes a duration event with End phase.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="name">The name of the event.</param>
        /// <param name="userData">The user data object.</param>
        public void EndEvent(TracingLevel level, string name, object userData)
        {
            if (IsTraceable(level))
            {
                Publish(new TracingEvent(level, TracingPhase.End, name, Name, GetThreadId(), userData));
            }
        }

        /// <summary>
        /// If the given level is allowed, it publishes the complete event with the time interval from the begin
        /// timestamp to now with the name.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="beginTimeStamp">The timestamp of the beginning of the event in microseconds.</param>
        /// <param name="name">The name of the event.</param>
        public void CompleteEvent(TracingLevel level, long beginTimeStamp, string name)
        {
            if (IsTraceable(level))
            {
                var duration = Timestamp - beginTimeStamp;
                Publish(
                    new TracingEvent(
                        level, TracingPhase.Complete, name, Name, beginTimeStamp, GetThreadId(), duration, null));
            }
        }

        /// <summary>
        /// If the given level is allowed, it publishes the complete event with the time interval from the begin
        /// timestamp to now with user data and the name.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="beginTimeStamp">The timestamp of the beginning of the event in microseconds.</param>
        /// <param name="name">The name of the event.</param>
        /// <param name="userData">The user data object.</param>
        public void CompleteEvent(TracingLevel level, long beginTimeStamp, string name, object userData)
        {
            if (IsTraceable(level))
            {
                var duration = Timestamp - beginTimeStamp;
                Publish(
                    new TracingEvent(
                        level, TracingPhase.Complete, name, Name, beginTimeStamp, GetThreadId(), duration, userData));
            }
        }

        /// <summary>
        /// If the given level is allowed, it publishes the complete event with the time interval from the begin
        /// timestamp to now with formatted name.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="beginTimeStamp">The timestamp of the beginning of the event in microseconds.</param>
        /// <param name="format">The name of the event in a composite format string.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public void CompleteEventFormat(TracingLevel level, long beginTimeStamp, string format, params object[] args)
        {
            if (IsTraceable(level))
            {
                var duration = Timestamp - beginTimeStamp;
                var name = string.Format(format, args);
                Publish(
                    new TracingEvent(
                        level, TracingPhase.Complete, name, Name, beginTimeStamp, GetThreadId(), duration, null));
            }
        }

        /// <summary>
        /// If the given level is allowed, it publishes the complete event with the time interval from the begin
        /// timestamp to now with user data and formatted name.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="beginTimeStamp">The timestamp of the beginning of the event in microseconds.</param>
        /// <param name="userData">The user data object.</param>
        /// <param name="format">The name of the event in a composite format string.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public void CompleteEventFormat(
            TracingLevel level, long beginTimeStamp, object userData, string format, params object[] args)
        {
            if (IsTraceable(level))
            {
                var duration = Timestamp - beginTimeStamp;
                var name = string.Format(format, args);
                Publish(
                    new TracingEvent(
                        level, TracingPhase.Complete, name, Name, beginTimeStamp, GetThreadId(), duration, userData));
            }
        }

        /// <summary>
        /// Creates a counter event, which can track a value as it changes over time.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <param name="name">The name of the event.</param>
        /// <param name="value">The traced value.</param>
        public void CounterEvent(TracingLevel level, string name, double value)
        {
            if (IsTraceable(level))
            {
                Publish(new TracingEvent(level, name, Name, GetThreadId(), value));
            }
        }

        /// <summary>
        /// Checks if an event of the given level would actually be traced by this tracer. This check is based on the
        /// Tracer effective level, which may be inherited from its parent.
        /// </summary>
        /// <param name="level">One of the event level identifiers, e.g. Severe.</param>
        /// <returns>True if the given event level is currently being traced.</returns>
        public bool IsTraceable(TracingLevel level)
        {
            if (Level != null)
            {
                return level >= Level;
            }
            else
            {
                if (Parent != null)
                {
                    return Parent.IsTraceable(level);
                }
                else
                {
                    return true;
                }
            }
        }

        private static int GetThreadId() => Thread.CurrentThread.ManagedThreadId;

        private void Publish(TracingEvent tracingEvent)
        {
            foreach (var handler in handlers)
            {
                if (handler.IsTraceable(tracingEvent))
                {
                    handler.Publish(tracingEvent);
                }
            }

            if (Parent != null && UseParentHandlers)
            {
                Parent.Publish(tracingEvent);
            }
        }

        private static class TracingTimestampProvider
        {
            private static long lastGetTimestamp = long.MinValue;

            private static readonly long offset = new DateTime(
                1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;

            /// <summary>
            /// Calculates the timestamp using the DateTime class.
            /// </summary>
            /// <returns>The number of microseconds since Unix epoch time.</returns>
            public static long GetDateTimeTimestamp()
            {
                var time = (DateTime.UtcNow.Ticks - offset) / 10;
                time = time > lastGetTimestamp ? time : lastGetTimestamp + 1;
                lastGetTimestamp = time;
                return time;
            }

            /// <summary>
            /// Calculates the timestamp using the Stopwatch class.
            /// </summary>
            /// <returns>The number of microseconds in the stopwatch timer mechanism.</returns>
            public static long GetStopwatchTimestamp()
            {
                var time = (1000 * 1000 * Stopwatch.GetTimestamp()) / Stopwatch.Frequency;
                time = time > lastGetTimestamp ? time : lastGetTimestamp + 1;
                lastGetTimestamp = time;
                return time;
            }
        }
    }
}