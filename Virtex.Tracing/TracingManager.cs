﻿using System;
using System.Collections.Generic;

namespace Virtex.Tracing
{
    /// <summary>
    /// There is a single global TracingManager object that is used to maintain tracers and handlers.
    /// </summary>
    /// <remarks>
    /// The global TracingManager object can be retrieved using property <see cref="Instace"/>. The TraceManager object is
    /// created during first access of Instance property. Tracers are organized into a naming hierarchy based on their
    /// dot separated names. Thus "a.b.c" is a child of "a.b", but "a.b1" and a.b2" are peers.
    /// </remarks>
    public class TracingManager
    {
        private static readonly Lazy<TracingManager> lazy = new Lazy<TracingManager>(() => new TracingManager());

        private readonly Dictionary<string, WeakReference<Tracer>> namesWithTracerReferences =
            new Dictionary<string, WeakReference<Tracer>>();

        private readonly object tracersLock = new object();

        /// <summary>
        /// Gets instance of the TracingManager object.
        /// </summary>
        /// <remarks>
        /// The TracingManager is a singleton object created during the first access of Instance property.
        /// </remarks>
        public static TracingManager Instace => lazy.Value;

        /// <summary>
        /// Removes and flushes all handlers and sets the Level to null for all named tracers and clears cached tracers.
        /// </summary>
        public void Reset()
        {
            lock (tracersLock)
            {
                List<string> namesToRemove = new List<string>();

                foreach (var nameWithTracerReference in namesWithTracerReferences)
                {
                    if (nameWithTracerReference.Value.TryGetTarget(out Tracer tracer))
                    {
                        tracer.Reset();
                    }
                    else
                    {
                        namesToRemove.Add(nameWithTracerReference.Key);
                    }
                }

                foreach (var name in namesToRemove)
                {
                    namesWithTracerReferences.Remove(name);
                }
            }
        }

        internal Tracer GetOrCreateTracer(string name)
        {
            lock (tracersLock)
            {
                Tracer tracer;
                if (namesWithTracerReferences.TryGetValue(name, out WeakReference<Tracer> tracerReference))
                {
                    if (!tracerReference.TryGetTarget(out tracer))
                    {
                        tracer = new Tracer(name);
                        namesWithTracerReferences[name].SetTarget(tracer);
                        OnTracerCreate(tracer);
                    }
                }
                else
                {
                    tracer = new Tracer(name);
                    namesWithTracerReferences.Add(name, new WeakReference<Tracer>(tracer));
                    OnTracerCreate(tracer);
                }
                return tracer;
            }
        }

        internal Tracer FindParent(Tracer tracer)
        {
            lock (tracersLock)
            {
                Tracer parent = null;

                foreach (var tracerReference in namesWithTracerReferences.Values)
                {
                    if (tracerReference.TryGetTarget(out Tracer newParent))
                    {
                        if (IsCloserParent(tracer, parent, newParent))
                        {
                            parent = newParent;
                        }
                    }
                }

                return parent;
            }
        }

        private static bool IsCloserParent(Tracer tracer, Tracer oldParent, Tracer newParent)
        {
            if (tracer != newParent && tracer.Name.Contains(newParent.Name))
            {
                return oldParent == null || newParent.Name.Length > oldParent.Name.Length;
            }

            return false;
        }

        private Tracer OnTracerCreate(Tracer tracer)
        {
            foreach (var nameWithTracerReference in namesWithTracerReferences)
            {
                if (nameWithTracerReference.Value.TryGetTarget(out Tracer possibleAffectedTracer))
                {
                    if (possibleAffectedTracer.Parent == tracer.Parent)
                    {
                        if (FindParent(possibleAffectedTracer) == tracer)
                        {
                            possibleAffectedTracer.Parent = tracer;
                        }
                    }
                }
            }
            return tracer;
        }
    }
}