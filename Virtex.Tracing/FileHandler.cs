﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Virtex.Tracing
{
    /// <summary>
    /// Stores formatted <see cref="TracingEvent"/>s in json file.
    /// </summary>
    public class FileHandler : TracingHandler
    {
        private readonly string folderPath;

        private readonly int splitCount;

        private readonly object writerLock = new object();

        private StreamWriter currentWriter;

        private int numEvents;

        private Task task;

        /// <summary>
        /// Gets the extension of a file where events are stored.
        /// </summary>
        protected virtual string FileExtension => "json";

        /// <summary>
        /// Initializes a new instance of <see cref="FileHandler"/> class.
        /// </summary>
        /// <param name="formatter">The formatter for this handler.</param>
        /// <param name="folderPath">The folder where to store json files.</param>
        /// <param name="splitCount">The number of formatted <see cref="TracingEvent"/>s per file.
        /// </param>
        public FileHandler(TracingFormatter formatter, string folderPath, int splitCount)
        {
            Formatter = formatter;
            this.folderPath = folderPath;
            this.splitCount = splitCount;
        }

        /// <summary>
        /// Format and publish a <see cref="TracingEvent"/> to internal memory stream.
        /// </summary>
        /// <param name="tracingEvent">The <see cref="TracingEvent"/></param>to publish.
        public override void Publish(TracingEvent tracingEvent)
        {
            lock (writerLock)
            {
                if (numEvents == splitCount)
                {
                    WaitUntilAllFlushAndCloseAreFinished();
                    var writerToFlush = currentWriter;
                    task = Task.Run(() => FlushAndClose(writerToFlush));
                    currentWriter = null;
                    numEvents = 0;
                }

                if (currentWriter == null)
                {
                    currentWriter = new StreamWriter(new MemoryStream());
                    currentWriter.Write(Formatter.Head);
                    currentWriter.Write(Formatter.Format(tracingEvent));
                }
                else
                {
                    currentWriter.Write(Formatter.Separator + Formatter.Format(tracingEvent));
                }

                numEvents++;
            }
        }

        /// <summary>
        /// Flushes and closes internal memory stream and write it to a json file. 
        /// </summary>
        public override void Flush()
        {
            lock (writerLock)
            {
                if (currentWriter != null)
                {
                    WaitUntilAllFlushAndCloseAreFinished();
                    FlushAndClose(currentWriter);
                    currentWriter = null;
                    numEvents = 0;
                }
            }
        }

        /// <summary>
        /// Writes data in given stream with given writer.
        /// </summary>
        /// <param name="writer">The writer to write data.</param>
        /// <param name="stream">The stream that contains data to write.</param>
        protected virtual void Write(StreamWriter writer, Stream stream)
        {
            writer.BaseStream.CopyTo(stream);
        }

        private void FlushAndClose(StreamWriter writer)
        {
            writer.Write(Formatter.Tail);
            writer.Flush();
            writer.BaseStream.Position = 0;

            var outputPath = Path.Combine(folderPath, $"{GetFileName()}.{FileExtension}");
            using (var stream = File.Create(outputPath))
            {
                Write(writer, stream);
            }

            writer.Close();
        }

        private void WaitUntilAllFlushAndCloseAreFinished()
        {
            // Just for rare cases when request for new FlushAndClose comes earlier than last one
            // is finished.
            while (task != null && !task.IsCompleted) { }
        }

        private static string GetFileName()
        {
            return DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-ffffff");
        }
    }
}

