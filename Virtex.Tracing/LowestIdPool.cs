﻿using System.Collections.Generic;
using System.Threading;

namespace Virtex.Tracing
{
    /// <summary>
    /// Provides the lowest unused IDs.
    /// </summary>
    public class LowestIdPool
    {
        private readonly SortedSet<int> emptyIds = new SortedSet<int>();

        private readonly SortedSet<int> occupiedIds = new SortedSet<int>();

        private readonly Dictionary<int, (int minimal, int stack)> managedToMinimal =
            new Dictionary<int, (int, int)>();

        private readonly object setLock = new object();

        /// <summary>
        /// Initializes a new instance of pool with lowest index 1.
        /// </summary>
        public LowestIdPool()
            : this(1)
        {
        }

        /// <summary>
        /// Initializes a new instance of pool with the minimal value.
        /// </summary>
        public LowestIdPool(int minimalValue)
        {
            emptyIds.Add(minimalValue);
        }

        /// <summary>
        /// Gets the lowest unused ID.
        /// </summary>
        /// <returns>The lowest unused ID.</returns>
        public int GetId()
        {
            var managedThreadId = Thread.CurrentThread.ManagedThreadId;
            lock (setLock)
            {
                if (managedToMinimal.TryGetValue(
                    managedThreadId, out (int mimimal, int stack) result))
                {
                    result.stack++;
                    managedToMinimal[managedThreadId] = result;
                    return result.mimimal;
                }
                else
                {
                    int min;
                    if (emptyIds.Count > 0)
                    {
                        min = emptyIds.Min;
                        emptyIds.Remove(min);
                    }
                    else
                    {
                        min = occupiedIds.Max + 1;
                    }
                    occupiedIds.Add(min);
                    managedToMinimal[managedThreadId] = (min, 1);
                    return min;
                }
            }
        }

        /// <summary>
        /// Return unused ID to pool. 
        /// </summary>
        /// <param name="id">The unused ID.</param>
        public void Return(int id)
        {
            var managedThreadId = Thread.CurrentThread.ManagedThreadId;
            lock (setLock)
            {
                if (occupiedIds.Remove(id))
                {
                    emptyIds.Add(id);
                }
                var minimal = managedToMinimal[managedThreadId];
                minimal.stack--;
                if (minimal.stack > 0)
                {
                    managedToMinimal[managedThreadId] = minimal;
                }
                else
                {
                    managedToMinimal.Remove(managedThreadId);
                }
            }
        }
    }
}