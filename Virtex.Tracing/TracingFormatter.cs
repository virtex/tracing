﻿namespace Virtex.Tracing
{
    /// <summary>
    /// Provides support for text formatting of <see cref="TracingEvent"/>.
    /// </summary>
    public abstract class TracingFormatter
    {
        /// <summary>
        /// Gets the header string for a set of formatted events.
        /// </summary>
        /// <remarks>
        /// Base implementation returns an empty string.
        /// </remarks>
        public virtual string Head => string.Empty;

        /// <summary>
        /// Gets the tail string for a set of formatted events.
        /// </summary>
        /// <remarks>
        /// Base implementation returns an empty string.
        /// </remarks>
        public virtual string Tail => string.Empty;

        /// <summary>
        /// Gets the separator string for a set of formatted events.
        /// </summary>
        /// <remarks>
        /// Base implementation returns an empty string.
        /// </remarks>
        public virtual string Separator => string.Empty;

        /// <summary>
        /// Formats the given <see cref="TracingEvent"/>.
        /// </summary>
        /// <param name="tracingEvent">The <see cref="TracingEvent"/> to format.</param>
        /// <returns>The formated <see cref="TracingEvent"/>.</returns>
        public abstract string Format(TracingEvent tracingEvent);
    }
}