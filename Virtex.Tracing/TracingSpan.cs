﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Virtex.Tracing
{
    /// <summary>
    /// Generates <see cref="TracingPhase.Complete"/> event with <see cref="TracingLevel.Fine"/>
    /// level for scope.
    /// </summary>
    public sealed class TracingSpan : IDisposable
    {
        private static readonly LowestIdPool threadIdGenerator = new LowestIdPool();

        private readonly Tracer tracer;

        private readonly TracingLevel level;

        private readonly string name;

        private readonly long beginTimestamp;

        private readonly object userData;

        private readonly int threadId =
            UseMinimalThreadIds ? threadIdGenerator.GetId() : Thread.CurrentThread.ManagedThreadId;

        /// <summary>
        /// Creates instance of <see cref="TracingSpan"/> with given arguments.
        /// </summary>
        /// <remarks>
        /// Use it as <see cref="IDisposable"/> object.
        /// using var s = new <see cref="TracingSpan"/>().
        /// </remarks>
        /// <param name="arguments">The list of arguments with name and value.</param>
        public TracingSpan(params (string name, object value)[] arguments)
        {
            var (spaceName, className, methodName) = ParseName(2);

            tracer = Tracer.GetTracer(spaceName ?? Tracer.RootTracerName);
            level = TracingLevel.Fine;
            name = $"{methodName} ({className})";
            userData = CreateUserData(arguments);
            beginTimestamp = Tracer.Timestamp;
        }

        /// <summary>
        /// Creates instance of <see cref="TracingSpan"/> with given tracer, level, name and
        /// user data.
        /// </summary>
        /// <param name="tracer">The tracer to trace a span.</param>
        /// <param name="level">One of the <see cref="TracingLevel"/>, e.g. Severe.</param>
        /// <param name="name">The name of the <see cref="TracingPhase.Complete"/> event.</param>
        /// <param name="userData">The user data associated with an event.</param>
        public TracingSpan(Tracer tracer, TracingLevel level, string name, object userData)
        {
            this.tracer = tracer;
            this.level = level;
            this.name = name;
            this.userData = userData;
            beginTimestamp = Tracer.Timestamp;
        }

        /// <summary>
        /// Gets or sets the flag for controlling of generating thread IDs.
        /// </summary>
        /// <remarks>
        /// If sets to true, IDs are generated with a minimal empty value,
        /// otherwise the real managed thread IDs are used. The default value is false.
        /// </remarks>
        public static bool UseMinimalThreadIds { get; set; } = false;

        /// <summary>
        /// The unique identification of current virtual thread.  
        /// </summary>
        public int ThreadId => threadId;

        /// <summary>
        /// Stores given value as _return_ property to Args and returns it.
        /// </summary>
        /// <typeparam name="T">The type of value to trace.</typeparam>
        /// <param name="value">The value to trace.</param>
        /// <returns>The value to trace.</returns>
        public T Return<T>(T value)
        {
            if (userData is Dictionary<string, string> dictionary)
            {
                dictionary.Add("_return_", $"{value}");
            }
            return value;
        }

        /// <summary>
        /// Automatically creates a <see cref="TracingPhase.Complete"/> event with parameters given
        /// in constructor.
        /// </summary>
        public void Dispose()
        {
            tracer.Event(
                new TracingEvent(
                    level,
                    TracingPhase.Complete,
                    name,
                    tracer.Name,
                    beginTimestamp,
                    threadId,
                    Tracer.Timestamp - beginTimestamp,
                    0,
                    userData));
            if (UseMinimalThreadIds)
            {
                threadIdGenerator.Return(ThreadId);
            }
        }

        private static (string spaceName, string className, string methodName) ParseName(int index)
        {
            var stackTrace = new StackTrace(false);
            StackFrame validFrame = null;
            while (validFrame is null && index < stackTrace.FrameCount)
            {
                var frame = stackTrace.GetFrame(index++);
                validFrame = frame.HasMethod() ? frame : null;
            }
            var method = validFrame?.GetMethod();
            return (method?.DeclaringType.Namespace, method?.DeclaringType.Name, method?.Name);
        }

        private static object CreateUserData(params (string name, object value)[] arguments)
        {
            var data = new Dictionary<string, string>();
            foreach (var arg in arguments)
            {
                data.Add(arg.name, FormattableString.Invariant($"{arg.value}"));
            }
            return data;
        }
    }
}
