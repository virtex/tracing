﻿using System;
using System.Globalization;
using System.Text;
using System.Text.Json;

namespace Virtex.Tracing
{
    /// <summary>
    /// Provides JSON formatting of <see cref="TracingEvent"/> for displaying in chrome://tracing tab of Chrome browser.
    /// </summary>
    public class ChromeFormatter : TracingFormatter
    {
        private readonly StringBuilder builder = new StringBuilder();

        /// <summary>
        /// Gets the header string for field of JSON formatted events.
        /// </summary>
        public override string Head => "{\"traceEvents\":[" + Environment.NewLine;

        /// <summary>
        /// Gets the tail string for field of JSON formatted events.
        /// </summary>
        public override string Tail
        {
            get
            {
                var tail = string.Empty;
                if (Tracer.ProcessName != string.Empty)
                {
                    tail += Separator;
                    tail += "{ ";
                    tail += "\"name\": \"" + "process_labels" + "\", ";
                    tail += "\"ph\": \"" + "M" + "\", ";
                    tail += "\"pid\": " + Tracer.ProcessId + ", ";
                    tail += "\"args\": {\"labels\": \"" + Tracer.ProcessName + "\"}";
                    tail += " }";
                }
                tail += Environment.NewLine + "]}";
                return tail;
            }
        }

        /// <summary>
        /// Gets the separator string for field of JSON formatted events.
        /// </summary>
        public override string Separator => "," + Environment.NewLine;

        /// <summary>
        /// Formats the given <see cref="TracingEvent"/> to JSON.
        /// </summary>
        /// <param name="tracingEvent">The <see cref="TracingEvent"/> to format.</param>
        /// <returns>Formatted <see cref="TracingEvent"/>.</returns>
        public override string Format(TracingEvent tracingEvent)
        {
            builder.Clear();

            builder.Append("{ ");
            builder.Append("\"cat\": \"" + tracingEvent.Category + "\", ");
            builder.Append("\"pid\": " + Tracer.ProcessId + ", ");
            builder.Append("\"tid\": " + tracingEvent.ThreadId + ", ");
            builder.Append("\"ts\": " + tracingEvent.Timestamp + ", ");
            builder.Append("\"ph\": \"" + (char)tracingEvent.Phase + "\", ");
            builder.Append("\"name\": \"" + tracingEvent.Name + "\", ");
            builder.Append("\"dur\": " + tracingEvent.Duration + ", ");

            if (tracingEvent.Phase == TracingPhase.Instant)
            {
                if (tracingEvent.Level >= TracingLevel.Warning)
                {
                    builder.Append("\"s\": \"p\", ");
                }
            }
            if (tracingEvent.Phase == TracingPhase.Counter)
            {
                var value = tracingEvent.Value.ToString(CultureInfo.InvariantCulture);
                builder.Append("\"args\": {\"value\": " + value + "}");
            }
            else
            {
                if (tracingEvent.UserData != null)
                {
                    var value = JsonSerializer.Serialize(tracingEvent.UserData);
                    builder.Append($"\"args\": {value}");
                }
                else
                {
                    builder.Append("\"args\": {}");
                }
            }

            builder.Append(" }");

            return builder.ToString();
        }
    }
}