﻿namespace Virtex.Tracing
{
    /// <summary>
    /// Represents object that is used to pass tracing requests between tracer and handlers.
    /// </summary>
    public readonly struct TracingEvent
    {
        /// <summary>
        /// Gets the level of the event, for example Level.Warning.
        /// </summary>
        public TracingLevel Level { get; }

        /// <summary>
        /// Gets the type of the event.
        /// </summary>
        public TracingPhase Phase { get; }

        /// <summary>
        /// Gets the name of the event.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the category of the event.
        /// </summary>
        public string Category { get; }

        /// <summary>
        /// Gets the time in microseconds since computer startup of the event.
        /// </summary>
        public long Timestamp { get; }

        /// <summary>
        /// Gets the managed thread Id of the event.
        /// </summary>
        public int ThreadId { get; }

        /// <summary>
        /// Gets the duration of the event.
        /// </summary>
        public long Duration { get; }

        /// <summary>
        /// Gets the value of the event.
        /// </summary>
        public double Value { get; }

        /// <summary>
        /// Gets the user data object associated with the event.
        /// </summary>
        public object UserData { get; }

        /// <summary>
        /// Initializes a new instance of tracing event struct.
        /// </summary>
        /// <param name="level">The level of the event.</param>
        /// <param name="phase">The phase of the event.</param>
        /// <param name="name">The name of the event.</param>
        /// <param name="category">The category of the event.</param>
        /// <param name="timestamp">The timestamp of the event.</param>
        /// <param name="threadId">The managed thread Id of the event.</param>
        /// <param name="duration">The duration of the event.</param>
        /// <param name="value">The value of the event.</param>
        /// <param name="userData">The user data object.</param>
        public TracingEvent(
            TracingLevel level,
            TracingPhase phase,
            string name,
            string category,
            long timestamp,
            int threadId,
            long duration,
            double value,
            object userData)
        {
            Level = level;
            Phase = phase;
            Name = name;
            Category = category;
            Timestamp = timestamp;
            ThreadId = threadId;
            Duration = duration;
            Value = value;
            UserData = userData;
        }

        internal TracingEvent(TracingLevel level, TracingPhase phase, string name, string category, int threadId)
            : this(level, phase, name, category, Tracer.Timestamp, threadId, 0, 0, null)
        {
        }

        internal TracingEvent(
            TracingLevel level, TracingPhase phase, string name, string category, int threadId, object userData)
            : this(level, phase, name, category, Tracer.Timestamp, threadId, 0, 0, userData)
        {
        }

        internal TracingEvent(TracingLevel level, string name, string category, int threadId, double value)
            : this(level, TracingPhase.Counter, name, category, Tracer.Timestamp, threadId, 0, value, null)
        {
        }

        internal TracingEvent(
            TracingLevel level,
            TracingPhase phase,
            string name,
            string category,
            long timestamp,
            int threadId,
            long duration,
            object userData)
            : this(level, phase, name, category, timestamp, threadId, duration, 0, userData)
        {
        }
    }
}