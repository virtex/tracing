﻿namespace Virtex.Tracing
{
    /// <summary>
    /// Determines a type of the event.
    /// </summary>
    public enum TracingPhase
    {
        /// <summary>
        /// Corresponds to something that happens but has no time interval associated with it.
        /// </summary>
        /// <remarks>
        /// For example, error reports are considered as instant events.
        /// </remarks>
        Instant = 'I',

        /// <summary>
        /// Represents beginning of a duration event on a given thread.
        /// </summary>
        /// <remarks>
        /// The duration events are specified by the Begin and End phase types. The Begin event must come before the
        /// corresponding End event. You can nest the Begin and End events. This allows you to capture function calling
        /// behavior on a thread. The timestamps for the duration events must be in increasing order for a given thread.
        /// </remarks>
        Begin = 'B',

        /// <summary>
        /// Represents end of a duration event on a given thread.
        /// </summary>
        /// <remarks>
        /// The duration events are specified by the Begin and End phase types. The Begin event must come before the
        /// corresponding End event. You can nest the Begin and End events. This allows you to capture function calling
        /// behavior on a thread. The timestamps for the duration events must be in increasing order for a given thread.
        /// </remarks>
        End = 'E',

        /// <summary>
        /// Combines a pair of duration (Begin and End) events.
        /// </summary>
        /// <remarks>
        /// Duration parameter is used to specify the duration of complete event in microseconds. The timestamp
        /// parameter indicate the time of the start of the complete event. Unlike duration events, the timestamps of
        /// duration events can be in any order.
        /// </remarks>
        Complete = 'X',

        /// <summary>
        /// Tracks a value as it changes over time.
        /// </summary>
        Counter = 'C'
    }
}