Virtex.Tracing
========================================

Virtex.Tracing provides profiling and logging infrastructure that helps you to locate errors, performance bottlenecks and create reports  
at run-time.


Download and Install Virtex.Tracing
----------------------------------------

The library is available on NuGet: https://www.nuget.org/packages/Virtex.Tracing/. Use the following command to install Tracing using  
NuGet package manager console:

	PM> Install-Package Virtex.Tracing

	
API
----------------------------------------

```Tracer``` creates ```TracingEvent``` and forward it to all registered ```TracingHandler``` objects.

	// Get a reference to Tracer object.
	// Common scenario is declare private static field and use convenient but slow method Tracer.GetCurrentNamespceTracer()
	var tracer = Tracer.GetTracer("Tracer name");

	// Add event handlers. E.g. ZipHandler with ChromeFormatter for offline visualization in Chrome browser.
	tracer.AddHandler(new ZipHandler(new ChromeFormatter(), @"C:\Tracer", 100_000));
	
	// Create events. E.g. a pair of duration events that measure time program spends in block of code.
	tracer.BeginEvent(Level.Fine, "Foo");
	Foo();
	tracer.EndEvent(Level.Fine, "Foo");
	
	// Flush any buffered output of all registered handlers. E.g. save formatted events to zip file.
	tracer.Flush();

The library can be extended by extending ```TracingHandler``` class to handle ```TracingEvent``` in any desirable way.


Chromium browsers
----------------------------------------

Events that are formatted with ```ChromeFormatter``` and stored in JSON file can be viewed in Chromium based browser.

- Open for e.g. Chrome browser.
- Write chrome://tracing to address bar.
- Click Load and select desired JSON file or archive.
- Click ? in the upper left corner to open the help window.

![Chrome](./Chrome.png "Detail of tracing tab in Chrome")