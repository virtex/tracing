1.1.0
--------------------
25 Sep 2024

#### Changes
- Customizable timestamp calculator (#56)
  - The `Tracer` class has a new property `TimestampCalculator` that allows setting a custom timestamp
	calculator. The default timestamp calculator is `DateTimeTimestampCalculator` and returns
    the number of microseconds since the Unix epoch.


1.0.1
--------------------    
13 May 2024

#### Patches
- License date updated.
- `PerformanceCreateEvent` unit test fixed.


1.0.0
--------------------
23 Apr 2024

#### Breaking changes
- Ambiguity for string user data (#55)
  - All the methods for generating events with formatted string have `Format` suffixes.
    - `InstantEvent(level, format, args)` method renamed to `InstantEventFormat`.
    - `InstantEvent(level, userData, format, args)` method renamed to `InstantEventFormat`.
  - `userData` and `name` arguments swapped in `InstantEvent(level, userData, name)`.

#### Patches
- Unique timestamps (#54)
  - Events that start at one microsecond on one thread are no longer improperly displayed
    in Chromium browsers.


0.9.0
--------------------
02 Aug 2023

#### Breaking changes
- User data serialization (#53)
  - Add dependency to `System.Text.Json 7.0.3` NuGet for serialization of user data.


0.8.0
--------------------
14 Jul 2023

#### Breaking changes
- UseMinimalThreadIds does not work for nested calls (#52)
  - Fixed typo in public property. `UseMininalThreadIds` renamed to `UseMinimalThreadIds`.
    Fixed reported bug.


0.7.2
--------------------
27 Oct 2022

#### Patches
- Global scoped events are sometime not visible in Chromium viewers (#50)
  - Events with Severe levels are formatted with process scope in `ChromeFormatter`,
    to not use global scope, which is bugged in Chromium browsers. The processor scope is not easy
    to see at first look, but on the other side, it is clear to which process the event belongs,
    which is useful when viewing traces from multiple services.


0.7.1
--------------------
13 Aug 2022

#### Patches
- Can't serialize empty dictionary (#49)
  - Output from `ChromeFormatter` is now valid JSON when an empty dictionary object is used as
   `userData` parameter when creating any trace event.


0.7.0
--------------------
27 Jul 2022

#### Features
- Process labels (#46)
  - Possibility to associate labels to process IDs to view the user names for specific
    processes or services in Chromium browsers.
- Optimize viewing of ASP .NET traces (#47)
  - `UseMininalThreadIds` static property added to `TracingSpan` class to allow minimizing number
    of displayed threads in Chomium browsers. Default behaviour was not chagned.
- Add possibility to debug Virtex.Tracing library.


0.6.1
--------------------
17 May 2022

#### Fixes
- Output of ChromeFormatter with specific user data can't be opened in Chrome browser (#44)
- Parallel creating of tracer crashes (#45)


0.6.0
--------------------
17 Nov 2021

#### Features
- File handler (#41)
  - Stores events in an uncompressed way. (Chrome browser can't open some of the zipped files.)
- Tracing Span (#42)
  - New class for a convenient way how to trace method with its arguments and return value.
- Root tracer property
  - Add static Root property to Tracer class for convenient way hot to obtain root tracer.

#### Fixes
- Ambiguous parameters in CompleteEvent method (#39)
  - Divide CompleteEvent methods to CompleteEvent and CompleteEventFormat.
- Zip handler sometimes crashes (#40)
  - Fix some cases when the underlying stream writer was garbage collected too early.

#### Breaking changes
- Change the signature of Tracer.CompleteEvent method and rename two of them to CompleteEventFormat.


0.5.2
--------------------
30 Dec 2019

#### Fixes
- Possible collision of parameters in overloaded Complete event (#38)


0.5.1
--------------------
04 Dec 2018

#### Fixes
- Fix wrong formatting of counter events that caused parsing error during a load of JSON file in  
Chrome browser in some cases. (#37)


0.5.0
--------------------
08 Oct 2018

#### Features
- ZipHandler and ChromeFormatter as a new way of preparing files for offline visualization (#21)
  - Zipped files occupy less than 10% of the original size
- CycleBufferHandler for precise profiling (#21)
  - Has to be used with target handler e.g. ZipHandler
  - Almost zero impact on performance (less than one microsecond per recorded event)
  - No garbage generation
- GetCurrentNamespaceTracer method as a convenient way of obtaining the tracer (#36)
- Override of TracingLevel ToString method to display name and value of level (#34)

#### Breaking changes
- Replace IHandler interface with TracingHandler abstract class (#21)
- Replace ChromeHandler with CycleBufferHandler, ZipHandler and ChromeFormatter (#21)
- Rename classes to avoid generic names (#34)
  - Level -> TracingLevel
  - Phase -> TracingPhase
  - TraceEvent -> TracingEvent
  - TracerManager -> TracingManager
- Add name parameter to TracingLevel constructor (#34)
- Add read-only modifier for all predefined levels e.g. TracingLevel.Fine (#34)
- Rename predefined tracing level name Sever to Severe (#34)


0.4.0
--------------------
02 Jun 2018

#### Features
- Event Categories - Every event contains Category property. The Category is set to Tracer's name  
  when the Tracer is used for the creation of an event. (#20)
- Chrome browser can display object tree for user data. (#33)

#### Fixes
- Chrome browser can’t load a record with new lines strings (#28)
- The Complete event does not accept user data (#29)
- Messed Instant event parameters (#30)
- Include XML documentation file in NuGet package (#32)

#### Breaking changes
- Category property added to TraceEvent constructor (#20)
- Parameters in InstantEvent method are swapped (#30)


0.3.1
--------------------
22 Mar 2018

#### Changes
- Severe instant events are visualized as thicker and longer lines. Warning instant events are  
  displayed as a bit thicker and longer lines

#### Fixes
- Fix crash during Flush of ChromHandler when an event does not contain user data
- Chrome browser can load record that contains Windows path separator character


0.3.0
--------------------
07 Feb 2018

#### Features
- TracerEvent can contain user data.
- ChromeHandler can serialize public properties of an object or all keys and values of  
  Dictionary<string, string>.
- New Tracer property to control whether events are passed to parent handlers or not.

#### Breaking changes
- Add UserData property to TraceEvent class


0.2.0
--------------------
30 Nov 2017

#### Features
- Each Tracer has a "Level" associated with it. This reflects a minimum Level that this tracer
  cares about.
- Tracers are normally named, using a hierarchical dot-separated namespace. Each Tracer keeps  
  track of a parent Tracer.
- Duration events provide a way to mark a duration of work on a given thread. The duration events  
  are specified by the Begin and End phase types.
- Counter event can track a value as it changes over time.

#### Breaking changes
- Add IsTraceable method to IHandler interface
- Remove Instance property from Tracer class
- Remove IsEnabled property from Tracer class
- Change signature of InstantEvent methods in Tracer class
- Change signature of CompleteEvent methods in Tracer class


0.1.1
--------------------
31 Oct 2017

#### Fixes
- Empty ChromeHandler does not create JSON file when is flushed.


0.1.0
----------------------------------------
30 Oct 2017

#### Features
- Tracer generates TraceEvents and provides core tracing functionality.
  - Instant event corresponds to something that happens but has no time interval associated with it.
  - Complete event corresponds to something that has time interval with Begin and End timestamp.
- ChromeHandler handles TraceEvents in a format suitable for offline visualization in Chrome browser.