using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Threading;

namespace Virtex.Tracing.Tests;

/// <summary>
/// Tests ChromeHandler class.
/// </summary>
[TestClass]
public class ChromeHandlerTest
{
    /// <summary>
    /// Tests Flush method.
    /// </summary>
    [TestMethod]
    public void ZipHandlerWithChromeFormatterFlush()
    {
        // arrange
        TracingManager.Instace.Reset();

        var tracerDirectory = GetNameAndPrepareChromeDirectory();

        var tracer = Tracer.GetTracer(Tracer.RootTracerName);
        var chromeHandler = new ZipHandler(new ChromeFormatter(), tracerDirectory, 1000);
        tracer.AddHandler(chromeHandler);

        var inputEvents = new List<TracingEvent>();
        foreach (var obj in Enum.GetValues(typeof(TracingPhase)))
        {
            for (int i = 0; i < 4; i++)
            {
                var phase = (TracingPhase)obj;
                TracingEvent inputEvent = CreateTestEvent(1000 - i * 100, phase, i % 2 == 1, i >= 2);

                inputEvents.Add(inputEvent);
                tracer.Event(inputEvent);
                Thread.Sleep(1);
            }
        }

        // act
        tracer.Flush();

        // assert
        var outputEvents = Load(Directory.GetFiles(tracerDirectory)[0]);

        for (int i = 0; i < inputEvents.Count; i++)
        {
            var inputEvent = inputEvents[i];
            var outputEvent = outputEvents[i];

            Assert.AreEqual(inputEvent.Duration, outputEvent.Duration);
            Assert.AreEqual(inputEvent.Name, outputEvent.Name);
            Assert.AreEqual(inputEvent.Phase, outputEvent.Phase);
            Assert.AreEqual(inputEvent.ThreadId, outputEvent.ThreadId);
            Assert.AreEqual(inputEvent.Timestamp, outputEvent.Timestamp);
            Assert.IsTrue(Math.Abs(inputEvent.Value - outputEvent.Value) < 10e-9);
        }
    }

    /// <summary>
    /// Tests Flush method.
    /// </summary>
    [TestMethod]
    public void ZipHandlerEmptyFlush()
    {
        // arrange
        TracingManager.Instace.Reset();

        var tracerDirectory = GetNameAndPrepareChromeDirectory();

        var tracer = Tracer.GetTracer(Tracer.RootTracerName);
        tracer.AddHandler(new ZipHandler(null, tracerDirectory, 10));

        // act
        tracer.Flush();

        // assert
        Assert.AreEqual(0, Directory.GetFiles(tracerDirectory).Length);
    }

    /// <summary>
    /// Tests Flush method.
    /// </summary>
    [TestMethod]
    public void ZipHandlerMultiFlush()
    {
        // arrange
        TracingManager.Instace.Reset();
        var tracer = Tracer.GetTracer(Tracer.RootTracerName);
        var tracerDirectory = GetNameAndPrepareChromeDirectory();
        var splitCount = 1000;
        var filesCount = 5;
        tracer.AddHandler(new ZipHandler(new ChromeFormatter(), tracerDirectory, splitCount));

        // act
        for (int i = 0; i <= splitCount * filesCount; i++)
        {
            tracer.InstantEvent(TracingLevel.Severe, "Instant Event");
        }

        // assert
        Thread.Sleep(1);
        Assert.AreEqual(filesCount, Directory.GetFiles(tracerDirectory).Length);
    }

    /// <summary>
    /// Tests performance of a creation and publishing of an event with <see cref="ZipHandler"/> and
    /// <see cref="TracingFormatter"/>.
    /// </summary>
    [TestMethod]
    public void PerformanceOfZipHandler()
    {
        // arrange
        TracingManager.Instace.Reset();
        var tracer = Tracer.GetTracer(Tracer.RootTracerName);
        var tracerDirectory = GetNameAndPrepareChromeDirectory();
        var numOfRuns = 100_000;
        tracer.AddHandler(new ZipHandler(new ChromeFormatter(), tracerDirectory, numOfRuns + 1_000));
        HeatUpTracer(tracer, 1_000);

        GC.Collect();
        var bts = Tracer.Timestamp;

        // act
        for (int i = 0; i < numOfRuns; i++)
        {
            tracer.InstantEvent(TracingLevel.Severe, "Instant Event");
        }

        // assert
        var durationNanos = (Tracer.Timestamp - bts) * 1_000 / numOfRuns;
        Console.WriteLine($"Create and publish an event with {nameof(ZipHandler)} takes {durationNanos}ns");
        Assert.IsTrue(durationNanos <= 2_000 * TracerTest.MachineHandicap);
    }

    [TestMethod]
    public void ManualQuotationOutput()
    {
        // arrange
        TracingManager.Instace.Reset();
        var tracer = Tracer.Root;
        var tracerDirectory = GetNameAndPrepareChromeDirectory();
        tracer.AddHandler(new FileHandler(new ChromeFormatter(), tracerDirectory, 10));

        var testUserData = new Dictionary<string, string> { { "test", "\"in quotas\"" } };

        // act
        tracer.InstantEvent(TracingLevel.Info, "Instant with args.", testUserData);

        // assert
        tracer.Flush();
    }

    [TestMethod]
    public void ManualNoUserData()
    {
        // arrange
        TracingManager.Instace.Reset();
        var tracer = Tracer.Root;
        var tracerDirectory = GetNameAndPrepareChromeDirectory();
        tracer.AddHandler(new FileHandler(new ChromeFormatter(), tracerDirectory, 10));

        // act
        tracer.InstantEvent(TracingLevel.Info, "Instant with Null args.");
        tracer.InstantEvent(
            TracingLevel.Info, "Instant with Empty args.", new Dictionary<string, string>());

        // assert
        tracer.Flush();
    }

    [TestMethod]
    public void ManualSpecificOutput()
    {
        // arrange
        TracingManager.Instace.Reset();
        var tracer = Tracer.Root;
        var tracerDirectory = GetNameAndPrepareChromeDirectory();
        tracer.AddHandler(new FileHandler(new ChromeFormatter(), tracerDirectory, 10));

        var testUserData = new TestUserData[]
        {
            new TestUserData(false, 10, 10.1, "abc"),
            new TestUserData(true,  20, 20.2, "def"),
        };
        var json1 = System.Text.Json.JsonSerializer.Serialize(testUserData);
        var json2 = "{\"Inputs\":[{\"Left\":false,\"Right\":true}]}";
        var json3 = "[{\"Left\":false,\"Right\":false}]";

        // act
        {
            using var s = new TracingSpan(
                ("id", "6a38076e-2630-40bf-a875-881d626ac9ce"),
                (nameof(json1), json1),
                (nameof(json2), json2),
                (nameof(json3), json3));
        }
        tracer.InstantEventFormat(TracingLevel.Info, "Instant without args.", new Dictionary<string, string>());

        // assert
        tracer.Flush();
    }

    private static void HeatUpTracer(Tracer tracer, int heatRuns)
    {
        for (int i = 0; i < heatRuns; i++)
        {
            tracer.InstantEvent(TracingLevel.All, "Instant Event");
        }
    }

    private static TracingEvent CreateTestEvent(
        int levelValue, TracingPhase phase, bool hasUserData, bool useDictionary)
    {
        var level = TracingLevel.Fine;
        if (levelValue >= TracingLevel.Severe.Value)
            level = TracingLevel.Severe;
        else if (levelValue >= TracingLevel.Warning.Value)
            level = TracingLevel.Warning;

        return new TracingEvent(
            level,
            phase,
            $"Test {phase} event",
            phase != TracingPhase.Counter ? new Random().NextDouble().ToString() : string.Empty,
            Tracer.Timestamp,
            Thread.CurrentThread.ManagedThreadId,
            phase == TracingPhase.Complete ? 100 : 0,
            phase == TracingPhase.Counter ? new Random().NextDouble() : 0,
            phase == TracingPhase.Counter ? null : (hasUserData ? useDictionary ?
            (object)new Dictionary<string, string>
            {
                ["BoolValue"] = false.ToString(),
                ["IntValue"] = 10.ToString(),
                ["DoubleValue"] = 12.3.ToString(CultureInfo.InvariantCulture)
            } : new TestUserData(true, 11, 23.4, "abc") : null));

    }

    private static TracingEvent[] Load(string sourceArchivePath)
    {
        FileInfo fileToDecompress = new FileInfo(sourceArchivePath);
        string decompressedPath = sourceArchivePath.Remove(
            sourceArchivePath.Length - fileToDecompress.Extension.Length);
        using (FileStream originalFileStream = fileToDecompress.OpenRead())
        {
            using FileStream decompressedFileStream = File.Create(decompressedPath);
            using var decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress);
            decompressionStream.CopyTo(decompressedFileStream);
        }

        string[] lines = File.ReadAllLines(decompressedPath);
        File.Delete(decompressedPath);

        var loadedEvents = new TracingEvent[lines.Length - 2];
        for (int i = 0; i < loadedEvents.Length; i++)
        {
            string[] data = lines[i + 1].Split("args\":");
            string[] defaultData = data[0].Split(',');
            string userData = data[1];

            TracingPhase phase = TracingPhase.Instant;
            string name = string.Empty;
            string category = string.Empty;
            long timestamp = 0;
            int threadId = 0;
            long duration = 0;
            double counterValue = 0;

            foreach (var rawAttribute in defaultData)
            {
                if (TryGetKeyValue(rawAttribute, out var key, out var value))
                {
                    switch (key)
                    {
                        case "name":
                            name = value;
                            break;
                        case "category":
                            category = value;
                            break;
                        case "ph":
                            switch (value)
                            {
                                case "I":
                                    phase = TracingPhase.Instant;
                                    break;
                                case "B":
                                    phase = TracingPhase.Begin;
                                    break;
                                case "E":
                                    phase = TracingPhase.End;
                                    break;
                                case "X":
                                    phase = TracingPhase.Complete;
                                    break;
                                case "C":
                                    phase = TracingPhase.Counter;
                                    break;
                            }
                            break;
                        case "tid":
                            threadId = int.Parse(value);
                            break;
                        case "ts":
                            timestamp = long.Parse(value);
                            break;
                        case "dur":
                            duration = long.Parse(value);
                            break;
                        default:
                            break;
                    }
                }
            }

            var namesWithValues = new Dictionary<string, string>();
            var args = userData.Split(',');
            foreach (var arg in args)
            {
                if (TryGetKeyValue(arg, out var propName, out var propValue))
                {
                    namesWithValues.Add(propName, propValue);
                }
            }
            object obj = null;
            if (namesWithValues.Count != 0)
            {
                if (phase == TracingPhase.Counter)
                {
                    counterValue = double.Parse(namesWithValues["value"], CultureInfo.InvariantCulture);
                }
                else
                {
                    obj = namesWithValues;
                }
            }

            loadedEvents[i] = new TracingEvent(TracingLevel.All, phase, name, category, timestamp, threadId,
                duration, counterValue, obj);
        }
        return loadedEvents;
    }

    private static bool Compare(object userData1, object userData2)
    {
        if (userData1 == null && userData2 == null)
            return true;

        if ((userData1 is Dictionary<string, string> userDictionary1) &&
            (userData2 is Dictionary<string, string> userDictionary2))
        {
            foreach (var vkp in userDictionary1)
            {
                var udValue =
                    userDictionary2[vkp.Key].Replace("true", "True").Replace("false", "False");
                if (vkp.Value != udValue)
                {
                    return false;
                }
            }
        }
        else
        {
            if (userData1 == null || userData2 == null)
                return false;

            var userObject = userData1 is Dictionary<string, string> ? userData2 : userData1;
            var userDictionary = userObject.Equals(userData1) ?
                userData2 as Dictionary<string, string> :
                userData1 as Dictionary<string, string>;

            foreach (var prop in userObject.GetType().GetProperties())
            {
                if (userDictionary.ContainsKey(prop.Name))
                {
                    var strValue =
                        userDictionary[prop.Name].Replace("true", "True").Replace("false", "False");
                    if (strValue != string.Format(CultureInfo.InvariantCulture, "{0}", prop.GetValue(userObject)))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        return true;
    }

    private static bool TryGetKeyValue(string text, out string key, out string value)
    {
        text = text.Trim('{', '}', ' ');
        var index = text.IndexOf(':');
        if (index >= 0)
        {
            key = text[..index].Trim('"', ' ');
            value = text.Remove(0, index + 1).Trim('"', ' ');
            return true;
        }
        else
        {
            key = value = string.Empty;
            return false;
        }
    }

    private static T CreateObject<T>(Dictionary<string, string> namesWithValues)
    {
        var obj = Activator.CreateInstance<T>();
        foreach (var nameWithValue in namesWithValues)
        {
            var prop = obj.GetType().GetProperty(nameWithValue.Key);
            if (prop != null)
            {
                if (prop.PropertyType == typeof(bool))
                {
                    prop.SetValue(obj, bool.Parse(nameWithValue.Value));
                }
                else if (prop.PropertyType == typeof(int))
                {
                    prop.SetValue(obj, int.Parse(nameWithValue.Value));
                }
                else if (prop.PropertyType == typeof(double))
                {
                    prop.SetValue(obj, double.Parse(nameWithValue.Value, CultureInfo.InvariantCulture));
                }
            }
        }
        return obj;
    }

    private static string GetNameAndPrepareChromeDirectory()
    {
        var tracerDirectory = Path.Combine(Environment.CurrentDirectory, "Tracer");

        Directory.CreateDirectory(tracerDirectory);
        foreach (var fileName in Directory.EnumerateFiles(tracerDirectory))
        {
            File.Delete(fileName);
        }

        return tracerDirectory;
    }
}