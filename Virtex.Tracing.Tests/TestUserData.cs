﻿namespace Virtex.Tracing.Tests;

internal class TestUserData
{
    public TestUserData(bool boolValue, int intValue, double doubleValue, string stringValue)
    {
        BoolValue = boolValue;
        IntValue = intValue;
        DoubleValue = doubleValue;
        StringValue = stringValue;
    }

    public TestUserData() : this(true, 0, 0, string.Empty)
    {
    }

    public bool BoolValue { get; set; }

    public int IntValue { get; set; }

    public double DoubleValue { get; set; }

    public string StringValue { get; set; }
}