﻿namespace Virtex.Tracing.Tests;

/// <summary>
/// Tracing handler for testing purposes.
/// </summary>
public class TestHandler : TracingHandler
{
    /// <summary>
    /// The last traced event.
    /// </summary>
    public TracingEvent LastEvent { get; private set; }

    public TracingEvent BeforeLastEvent { get; private set; }

    /// <summary>
    /// Does nothing.
    /// </summary>
    public override void Flush()
    {
    }

    /// <summary>
    /// Sets the event as <see cref="LastEvent"/> of the handler.
    /// </summary>
    /// <param name="traceEvent">The event to store.</param>
    public override void Publish(TracingEvent traceEvent)
    {
        BeforeLastEvent = LastEvent;
        LastEvent = traceEvent;
    }
}