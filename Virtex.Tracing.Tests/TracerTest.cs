using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Virtex.Tracing.Tests;

/// <summary>
/// Tests Tracer class.
/// </summary>
[TestClass]
public class TracerTest
{
    // Handicaps for testing tracer performance (higher handicap prolongs time limits).
    // Handicap 1 ~ 3GHz local machine
    // Handicap 10 ~ GitLab CI machine
    public static readonly long MachineHandicap = 10;

    /// <summary>
    /// Tests GetTracer method to return the same Tracer for the same name and different Tracer for the different
    /// name.
    /// </summary>
    [TestMethod]
    public void GetTracer()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();
        var aFst = Tracer.GetTracer("a");

        // act
        var aSnd = Tracer.GetTracer("a");
        var b = Tracer.GetTracer("b");

        // assert
        Assert.AreEqual(aFst, aSnd);
        Assert.AreNotEqual(aFst, b);
    }

    [TestMethod]
    public void GetSameTracerInParallel()
    {
        // arrange
        TracingManager.Instace.Reset();

        // act
        var tasks = new List<Task>();
        for (int i = 0; i < 100; i++)
        {
            tasks.Add(
                Task.Run(() =>
                {
                    var t = Tracer.GetCurrentNamespaceTracer();
                    t.InstantEvent(TracingLevel.Info, "test");
                }));
        }
        Task.WaitAll([.. tasks]);

        // assert
        Assert.AreEqual(GetType().Namespace, Tracer.GetCurrentNamespaceTracer().Name);
    }

    [TestMethod]
    public void GetCurrentNamespaceTracerWithType()
    {
        // act
        var tracer = Tracer.GetCurrentNamespaceTracer(GetType());

        // assert
        Assert.AreEqual(tracer.Name, GetType().Namespace);
    }

    [TestMethod]
    public void GetCurrentNamespaceTracer()
    {
        // act
        var tracer = Tracer.GetCurrentNamespaceTracer();

        // assert
        Assert.AreEqual(tracer.Name, GetType().Namespace);
    }

    /// <summary>
    /// Tests Parent property when a new Tracer is created inside Tracer's hierarchy.
    /// </summary>
    [TestMethod]
    public void AddParent()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();
        var ab = Tracer.GetTracer("a.b");

        // act
        var a = Tracer.GetTracer("a");

        // assert
        Assert.AreEqual(a, ab.Parent);
        Assert.AreNotEqual(Tracer.GetTracer(Tracer.RootTracerName), ab.Parent);
    }

    /// <summary>
    /// Tests Parent property when a Tracer with child disappear from Tracer's hierarchy.
    /// </summary>
    [TestMethod]
    public void RemoveParent()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();
        var root = Tracer.GetTracer(Tracer.RootTracerName);
        var ab = Tracer.GetTracer("a.b");

        // act
        CreateAndForgetTracer("a");
        GC.Collect();

        // assert
        Assert.AreEqual(root, ab.Parent);
    }

    /// <summary>
    /// Tests whether events are correctly propagated through Tracer's hierarchy.
    /// </summary>
    [TestMethod]
    public void Publish()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();

        var root = Tracer.GetTracer(Tracer.RootTracerName);
        var testHandler = new TestHandler();
        root.AddHandler(testHandler);
        var child = Tracer.GetTracer("a");
        var beforeEvent = testHandler.LastEvent;

        // act
        child.InstantEvent(TracingLevel.All, "test");

        // assert
        Assert.AreNotEqual(beforeEvent, testHandler.LastEvent);
    }

    /// <summary>
    /// Tests whether events are not passed to parent handlers when passing is disabled.
    /// </summary>
    [TestMethod]
    public void NotUseParentHandler()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();

        var root = Tracer.GetTracer(Tracer.RootTracerName);
        var testHandler = new TestHandler();
        root.AddHandler(testHandler);
        var child = Tracer.GetTracer("a");
        child.UseParentHandlers = false;
        var beforeEvent = testHandler.LastEvent;

        // act
        child.InstantEvent(TracingLevel.All, "test");

        // assert
        Assert.AreEqual(beforeEvent, testHandler.LastEvent);
    }

    /// <summary>
    /// Tests whether Instant event method with string format creates correct tracing event.
    /// </summary>
    [TestMethod]
    public void InstantEventWithFormat()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();

        var root = Tracer.GetTracer(Tracer.RootTracerName);
        var testHandler = new TestHandler();
        root.AddHandler(testHandler);
        var name = "Test {0}";
        var data = new object();

        // act
        root.InstantEventFormat(TracingLevel.All, name, data);

        // assert
        Assert.AreEqual(testHandler.LastEvent.Name, string.Format(name, data));
    }

    [TestMethod]
    public void CompleteEventWithFormat()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();

        var root = Tracer.GetTracer(Tracer.RootTracerName);
        var testHandler = new TestHandler();
        root.AddHandler(testHandler);
        var name = "Test {0}";
        var value = 10;

        // act
        root.CompleteEventFormat(TracingLevel.All, 0, name, value);

        // assert
        Assert.AreEqual(testHandler.LastEvent.Name, string.Format(name, value));
    }

    [TestMethod]
    public void CompleteEventWithFormatAndUserData()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();

        var root = Tracer.GetTracer(Tracer.RootTracerName);
        var testHandler = new TestHandler();
        root.AddHandler(testHandler);
        var name = "Test {0}";
        var value = 10;
        var data = new TestUserData(false, new Random().Next(), new Random().NextDouble(), "a");

        // act
        root.CompleteEventFormat(TracingLevel.All, 0, data, name, value);

        // assert
        Assert.AreEqual(testHandler.LastEvent.Name, string.Format(name, value));
        Assert.AreEqual(testHandler.LastEvent.UserData, data);
    }

    [TestMethod]
    public void CompleteEventWithUserData()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();

        var root = Tracer.GetTracer(Tracer.RootTracerName);
        var testHandler = new TestHandler();
        root.AddHandler(testHandler);
        var name = "Test {0}";
        var data = new TestUserData(false, new Random().Next(), new Random().NextDouble(), "a");

        // act
        root.CompleteEvent(TracingLevel.All, 0, name, data);

        // assert
        Assert.AreEqual(testHandler.LastEvent.UserData, data);
    }

    /// <summary>
    /// Tests performance of creation of an event.
    /// </summary>
    [TestMethod]
    public void PerformanceCreateEvent()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();
        var tracer = Tracer.GetTracer(Tracer.RootTracerName);
        var bts = DateTime.Now;
        var count = 1_000_000;

        // act
        for (int i = 0; i < count; i++)
        {
            tracer.InstantEvent(TracingLevel.Severe, "Performance Test");
        }

        // assert
        var durationNanos = (long)((DateTime.Now - bts).TotalMicroseconds * 1_000) / count;
        Console.WriteLine($"Create an event takes {durationNanos} ns");
        Assert.IsTrue(durationNanos <= 100 * MachineHandicap);
    }

    /// <summary>
    /// Tests performance of a discarded event.
    /// </summary>
    [TestMethod]
    public void PerformanceDiscardEvent()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();
        var tracer = Tracer.GetTracer(Tracer.RootTracerName);
        tracer.Level = TracingLevel.Off;
        var bts = Tracer.Timestamp;

        // act
        for (int i = 0; i < 1000 * 1000; i++)
        {
            tracer.InstantEvent(TracingLevel.Severe, "Performance Test");
        }

        // assert
        var durationNanos = (Tracer.Timestamp - bts) / 1000;
        Console.WriteLine($"Discard an event takes {durationNanos} ns");
        Assert.IsTrue(durationNanos <= 10 * MachineHandicap);
    }

    /// <summary>
    /// Tests performance of a discarded event with non-constant string.
    /// </summary>
    [TestMethod]
    public void PerformanceDiscardFormatEvent()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();
        var tracer = Tracer.GetTracer(Tracer.RootTracerName);
        tracer.Level = TracingLevel.Off;
        var bts = Tracer.Timestamp;

        // act
        for (int i = 0; i < 1000 * 1000; i++)
        {
            tracer.InstantEventFormat(TracingLevel.Severe, "Performance Test {0}", i);
        }

        // assert
        var durationNanos = (Tracer.Timestamp - bts) / 1000;
        Console.WriteLine($"Discard an event takes {durationNanos} ns");
        Assert.IsTrue(durationNanos <= 20 * MachineHandicap);
    }

    /// <summary>
    /// Tests performance of traverse one level in tracer hierarchy.
    /// </summary>
    [TestMethod]
    public void PerformanceTraverseHierarchy()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();
        var root = Tracer.GetTracer(Tracer.RootTracerName);
        root.Level = TracingLevel.Off;
        var leaf = Tracer.GetTracer("leaf");

        // act
        var btsRoot = Tracer.Timestamp;
        for (int i = 0; i < 1000 * 1000; i++)
        {
            root.InstantEvent(TracingLevel.Severe, "Performance Test");
        }
        var durationRootNanos = (Tracer.Timestamp - btsRoot) / 1000;

        var btsLeaf = Tracer.Timestamp;
        for (int i = 0; i < 1000 * 1000; i++)
        {
            leaf.InstantEvent(TracingLevel.Severe, "Performance Test");
        }
        var durationHierarchyNanos = (Tracer.Timestamp - btsLeaf) / 1000 - durationRootNanos;

        // assert
        Console.WriteLine($"Traverse an event throw one level in hierarchy takes {durationHierarchyNanos}ns");
        Assert.IsTrue(durationHierarchyNanos <= 12 * MachineHandicap);
    }

    private static void CreateAndForgetTracer(string name)
    {
        Tracer.GetTracer(name);
    }
}