﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Virtex.Tracing.Tests;

/// <summary>
/// Tests <see cref="TracingSpan"/> class.
/// </summary>
[TestClass]
public class TracingSpanTests
{
    [TestMethod]
    public void MinimalIdFromOneThread()
    {
        // arrange
        TracingManager.Instace.Reset();
        var root = Tracer.GetTracer(Tracer.RootTracerName);
        TracingSpan.UseMinimalThreadIds = true;
        var testHandler = new TestHandler();
        root.AddHandler(testHandler);

        // act
        {
            using var s = new TracingSpan();
            {
                using var t = new TracingSpan();
            }
        }

        // assert
        Assert.AreEqual(testHandler.LastEvent.ThreadId, testHandler.BeforeLastEvent.ThreadId);
    }

    [TestMethod]
    public void MinimalIdFromTwoThread()
    {
        // arrange
        TracingManager.Instace.Reset();
        var root = Tracer.GetTracer(Tracer.RootTracerName);
        TracingSpan.UseMinimalThreadIds = true;
        var testHandler = new TestHandler();
        root.AddHandler(testHandler);

        // act
        {
            Task t;
            using var s = new TracingSpan();
            {
                t = Task.Run(() => { using var s = new TracingSpan(); });
            }
            Task.WaitAll(t);
        }

        // assert
        Assert.AreNotEqual(
            testHandler.LastEvent.ThreadId, testHandler.BeforeLastEvent.ThreadId);
    }

    [TestMethod]
    public void GeneratesEventWithUserData()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();

        var root = Tracer.GetTracer(Tracer.RootTracerName);
        var testHandler = new TestHandler();
        root.AddHandler(testHandler);

        (string name, int value) argument = ("x", 2);

        // act
        {
            using var s = new TracingSpan(argument);
        }

        // assert
        Assert.AreEqual(testHandler.LastEvent.Name,
            $"{nameof(GeneratesEventWithUserData)} ({nameof(TracingSpanTests)})");
        Assert.AreEqual(testHandler.LastEvent.Category, "Virtex.Tracing.Tests");
        var userData = testHandler.LastEvent.UserData as Dictionary<string, string>;
        Assert.AreEqual(int.Parse(userData[argument.name]), argument.value);
    }

    [TestMethod]
    public void GeneratesEventWithComplexUserData()
    {
        // arrange
        GC.Collect();
        TracingManager.Instace.Reset();

        var root = Tracer.GetTracer(Tracer.RootTracerName);
        var testHandler = new FileHandler(new ChromeFormatter(), Path.GetTempPath(), 10);
        root.AddHandler(testHandler);

        (string name, object value)[] arguments =
        {
            ("int arg", 10),
            ("str arg", "0-hello"),
            ("obj arg", new TestUserData(false, 10, 10.1, "abc")),
        };

        // act
        {
            using var s = new TracingSpan(arguments);
        }

        // assert
        root.Flush();
    }
}